with import <nixpkgs> {};

mkShell rec {
    name = "sdl_development";

    buildInputs = [] ++ lib.optionals stdenv.isLinux [
        SDL2
        SDL2_image
    ];

    LD_LIBRARY_PATH = lib.makeLibraryPath buildInputs;
}
