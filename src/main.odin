package main

import "core:fmt"
import "core:math"
import SDL "vendor:sdl2"
import SDLImage "vendor:sdl2/image"

WINDOW_FLAGS       :: SDL.WINDOW_SHOWN
RENDER_FLAGS       :: SDL.RENDERER_ACCELERATED
TARGET_DT          :: 1000 / 60
WINDOW_WIDTH       :: 1280
WINDOW_HEIGHT      :: 720
CAMERA_PAN_SPEED   :: 200
CAMERA_ZOOM_AMOUNT :: 0.1

Camera :: struct {
    x, y, zoom, target_zoom : f64
}

Game :: struct {
    perf_frequency : f64,
    renderer       : ^SDL.Renderer,
    camera         : ^Camera,
    
    pan_camera            : bool,
    left, right, up, down : bool,
    mouse_x, mouse_y      : i32,

    ship : Entity
}

Entity :: struct {
    texture   : ^SDL.Texture,
    dest_rect :  SDL.Rect
}

game := Game{ 
    camera = &Camera{ zoom = 1, target_zoom = 1 }, 
    mouse_x = WINDOW_WIDTH / 2, 
    mouse_y = WINDOW_HEIGHT / 2,
}

get_time :: proc() -> f64 { return f64(SDL.GetPerformanceCounter()) * 1000 / game.perf_frequency }

main :: proc() {
    assert(SDL.Init(SDL.INIT_VIDEO) == 0, SDL.GetErrorString())
    assert(SDLImage.Init(SDLImage.INIT_PNG) != nil, SDL.GetErrorString())
    defer SDL.Quit()

    window := SDL.CreateWindow(
        "SDL2 Demo", 
        SDL.WINDOWPOS_CENTERED, 
        SDL.WINDOWPOS_CENTERED, 
        WINDOW_WIDTH, 
        WINDOW_HEIGHT, 
        WINDOW_FLAGS,
    )
    assert(window != nil, SDL.GetErrorString())
    defer SDL.DestroyWindow(window)

    game.renderer = SDL.CreateRenderer(window, -1, RENDER_FLAGS)
    assert(game.renderer != nil, SDL.GetErrorString())
    defer SDL.DestroyRenderer(game.renderer)

    ship_texture := SDLImage.LoadTexture(game.renderer, "assets/ship.png")
    assert(ship_texture != nil, SDL.GetErrorString())

    tiles_texture := SDLImage.LoadTexture(game.renderer, "assets/tiles.png")
    assert(tiles_texture != nil, SDL.GetErrorString())

    tiles_rect := 
    SDL.QueryTexture(tiles_texture, nil, nil, nil, nil)

    ship_rect := SDL.Rect{ x = 20, y = 40 }
    SDL.QueryTexture(ship_texture, nil, nil, &ship_rect.w, &ship_rect.h)

    game.perf_frequency = f64(SDL.GetPerformanceFrequency())
    start, end: f64

    event : SDL.Event
    state := SDL.GetKeyboardState(nil)

    game_loop : for {
        start = get_time()

        for SDL.PollEvent(&event) {
            #partial switch event.type {
            case SDL.EventType.QUIT:
                break game_loop
            case SDL.EventType.KEYDOWN:
                #partial switch event.key.keysym.scancode {
                case .ESCAPE:
                    break game_loop
                }
            case SDL.EventType.MOUSEBUTTONDOWN, SDL.EventType.MOUSEBUTTONUP:
                if (event.button.button == SDL.BUTTON_MIDDLE) {
                    game.pan_camera = event.type == SDL.EventType.MOUSEBUTTONDOWN
                }
            case SDL.EventType.MOUSEMOTION:
                if (game.pan_camera) {
                    game.camera.x += f64(event.motion.xrel) * (1 / game.camera.zoom)
                    game.camera.y += f64(event.motion.yrel) * (1 / game.camera.zoom)
                }

                game.mouse_x = event.motion.x
                game.mouse_y = event.motion.y
            case SDL.EventType.MOUSEWHEEL:
                game.camera.target_zoom += f64(event.wheel.y) * CAMERA_ZOOM_AMOUNT
                game.camera.target_zoom = clamp(game.camera.target_zoom, 0.1, 2)
            } 
        }

        delta: f64 = f64(TARGET_DT) / 1000

        game.camera.zoom = math.lerp(game.camera.zoom, game.camera.target_zoom, delta * 10)
        
        game.left  = state[SDL.Scancode.A] == 1 || state[SDL.Scancode.LEFT]  == 1
        game.right = state[SDL.Scancode.D] == 1 || state[SDL.Scancode.RIGHT] == 1
        game.up    = state[SDL.Scancode.W] == 1 || state[SDL.Scancode.UP]    == 1
        game.down  = state[SDL.Scancode.S] == 1 || state[SDL.Scancode.DOWN]  == 1
        
        game.camera.x += f64(i32(game.left) - i32(game.right)) * CAMERA_PAN_SPEED * delta * game.camera.zoom
        game.camera.y += f64(i32(game.up)   - i32(game.down) ) * CAMERA_PAN_SPEED * delta * game.camera.zoom

        // game.camera.x -= f64(clamp(game.mouse_x - WINDOW_WIDTH  + WINDOW_WIDTH  - 25, -25, 0) + clamp(game.mouse_x - WINDOW_WIDTH  + 25, 0, 25)) * delta
        // game.camera.y -= f64(clamp(game.mouse_y - WINDOW_HEIGHT + WINDOW_HEIGHT - 25, -25, 0) + clamp(game.mouse_y - WINDOW_HEIGHT + 25, 0, 25)) * delta
        
        RenderCopy(game.renderer, tiles_texture, &SDL.Rect{ x = 32, y = 0, w = 32, h = 32 }, &SDL.Rect{ x = 300, y = 300, w = 128, h = 128 })
        RenderCopy(game.renderer, tiles_texture, &SDL.Rect{ x = 32, y = 0, w = 32, h = 32 }, &SDL.Rect{ x = 300 + 128, y = 300, w = 128, h = 128 })
        RenderCopy(game.renderer, tiles_texture, &SDL.Rect{ x = 32, y = 0, w = 32, h = 32 }, &SDL.Rect{ x = 300, y = 300 + 128, w = 128, h = 128 })
        RenderCopy(game.renderer, tiles_texture, &SDL.Rect{ x = 32, y = 0, w = 32, h = 32 }, &SDL.Rect{ x = 300 + 128, y = 300 + 128, w = 128, h = 128 })
        RenderCopy(game.renderer, ship_texture, nil, &ship_rect)
        
        end = get_time()
        for end - start < TARGET_DT {
            end = get_time()
        }

        // fmt.println("FPS: ", 1000 / (end - start))

        SDL.RenderPresent(game.renderer)
        SDL.SetRenderDrawColor(game.renderer, 0, 0, 0, 0)
        SDL.RenderClear(game.renderer)
    }
}

RenderCopy :: proc(renderer: ^SDL.Renderer, texture: ^SDL.Texture, source_rect, dest_rect: ^SDL.Rect) {
    c_rect := SDL.Rect{ 
        x = i32((f64(dest_rect.x) + game.camera.x) * game.camera.zoom + f64(WINDOW_WIDTH / 2)), 
        y = i32((f64(dest_rect.y) + game.camera.y) * game.camera.zoom + f64(WINDOW_HEIGHT / 2)), 
        w = i32(f64(dest_rect.w) * game.camera.zoom), 
        h = i32(f64(dest_rect.h) * game.camera.zoom), 
    }
    SDL.RenderCopy(renderer, texture, source_rect, &c_rect)
}
